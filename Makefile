CI_COMMIT_SHORT_SHA ?=1.0.0-SNAPSHOT

build:
	mvn install:install-file -Dfile=lib/ojdbc6-11.2.0.3.jar -DpomFile=lib/ojdbc6-11.2.0.3.pom
	mvn versions:set -DnewVersion=$(CI_COMMIT_SHORT_SHA)
	mvn clean install
	rm -rf dbtool
	cp -r resources/ dbtool/
	cp target/dbtool-$(CI_COMMIT_SHORT_SHA)-jar-with-dependencies.jar   dbtool/dbtool-$(CI_COMMIT_SHORT_SHA).jar
	tar -czvf dbtool.tar.gz dbtool
