DB_SYSTEM_USER=		"${db_user}"
DB_SYSTEM_PASSWORD=	"${db_password}"
DB_SYSTEM_URL=		"${db_url}"
DB_SCHEMA_PREFIX=   ""
DBTOOL_CMD=			java -jar dbtool*.jar \
					-u=$(DB_SYSTEM_USER) \
					-p=$(DB_SYSTEM_PASSWORD) \
					-r=$(DB_SYSTEM_URL) \
					-m=$(DB_MAX_RETRIES)

#---------------------------------------------------------
db_check=		echo "dummy db is checked"	
db_initialized=$(DBTOOL_CMD) $(DB_SCHEMA_PREFIX)$(ARG_TEST_TABLE)
db_create=		echo "dummy create"
db_drop=		echo "dummy drop"
