DB_SYSTEM_USER=		"sa"
DB_SYSTEM_PASSWORD= "P@ssw0rd"
DB_SYSTEM_URL=		"$(shell echo $(db_url) | rev | cut -d/ -f2-10 | rev)/master"
DB_SCHEMA_PREFIX=	"$(db_schema)."
DBTOOL_CMD= 		java -jar dbtool*.jar \
					-u=$(DB_SYSTEM_USER) \
					-p=$(DB_SYSTEM_PASSWORD) \
					-r=$(DB_SYSTEM_URL) \
					-m=$(DB_MAX_RETRIES)

define DB_CREATE
	USE master;
	create database ${db_schema};
	ALTER DATABASE ${db_schema} SET READ_COMMITTED_SNAPSHOT ON WITH ROLLBACK IMMEDIATE;
	CREATE LOGIN ${db_user} WITH PASSWORD='${db_password}', DEFAULT_DATABASE=${db_schema}, CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;
	use ${db_schema};
	CREATE USER ${db_user} FOR LOGIN ${db_user};
	create schema ${db_schema} AUTHORIZATION ${db_user};
	GRANT all ON DATABASE :: ${db_schema} TO ${db_user} ;
	alter user ${db_user} with default_schema = ${db_schema};

endef
export DB_CREATE

define DB_DROP
	USE master;
	ALTER DATABASE ${db_schema} SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
	DROP DATABASE ${db_schema};
endef
export DB_DROP

#---------------------------------------------------------
db_check=$(DBTOOL_CMD)
db_initialized=$(DBTOOL_CMD) $(DB_SCHEMA_PREFIX)$(ARG_TEST_TABLE)
db_create=$(DBTOOL_CMD) -f create.sql
db_drop=$(DBTOOL_CMD) -f drop.sql
