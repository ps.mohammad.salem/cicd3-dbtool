DB_SYSTEM_USER=		"system"
DB_SYSTEM_PASSWORD= "oracle"
DB_SYSTEM_URL=		"$(db_url)"
DB_SCHEMA_PREFIX=	"$(db_schema)."
DBTOOL_CMD=			java -jar dbtool*.jar \
					-u=$(DB_SYSTEM_USER) \
					-p=$(DB_SYSTEM_PASSWORD) \
					-r=$(DB_SYSTEM_URL) \
					-m=$(DB_MAX_RETRIES)

define DB_CREATE
	ALTER SYSTEM SET db_securefile=NEVER SCOPE=BOTH; 
	CREATE USER ${db_schema} IDENTIFIED BY ${db_password}; 
	GRANT CONNECT, RESOURCE, CREATE VIEW, CREATE SYNONYM, UNLIMITED TABLESPACE TO ${db_schema};
	ALTER USER ${db_schema} QUOTA UNLIMITED ON SYSTEM;
endef
export DB_CREATE

define DB_DROP
	ALTER SYSTEM SET db_securefile=NEVER scope=both; 
	call KILL_USER_SESSION('${db_schema}');
	DROP USER ${db_schema} cascade;
endef
export DB_DROP

#---------------------------------------------------------
db_check=$(DBTOOL_CMD)
db_initialized=$(DBTOOL_CMD) $(DB_SCHEMA_PREFIX)$(ARG_TEST_TABLE)
db_create=$(DBTOOL_CMD) -f create.sql
db_drop=$(DBTOOL_CMD) -f drop.sql
