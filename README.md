dbtool is a utility to test and execute database commands
To use the dbtool lib please set db connection information inside enviroment variables 
```
export db_url="jdbc:**********"
export db_driver="org.h2.Driver" 
export db_user="" 
export db_password=""
```
dbtool provide three functionalities as following :

- Test if database engine is installed and ready
```
java -jar dbtool-1.0-jar-with-dependencies.jar
```
- Test if database is initialized 
```
java -jar dbtool-1.0-jar-with-dependencies.jar [tableName]
```
- Execute SQL command from file  
```
java -jar dbtool-1.0-jar-with-dependencies.jar -f [sql file path]
```

All functions will return true in case of success and false in case of failure .
