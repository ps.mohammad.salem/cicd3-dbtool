package com.progressoft.genesis.dbtool;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.sql.*;

public class Test {

    static String dbUser;
    static String dbPassword;
    static String dbUrl;
    public static String dbDriver;

    static int db_check_retry_sleepTime=5000;
    static int db_check_retry_maxCount=5;
    static int db_check_retry_count=0;

    public static void main(String[] args) throws Exception {
        try {
            checkCommand(args);
            runCommand(args);
            System.out.println("true");
            System.exit(0);
        } catch (Exception e ) {
            System.out.println("\n"+e.getMessage());
            System.out.println("false");
            System.exit(1);
        }
    }

    private static void checkCommand(String[] args)  throws Exception{
        if(args.length<4) {
            System.out.println("Error no username,password,dbUrl and max_retry_count passed");
            throw new Exception();
        }
    }

    private static void runCommand(String[] args) throws Exception{
        dbUser=args[0].replace("-u=","");
        dbPassword=args[1].replace("-p=","");
        dbUrl=args[2].replace("-r=","");
        db_check_retry_maxCount=Integer.parseInt(args[3].replace("-m=",""));
        if(args.length==4){
            checkDBReadiness();
        }else if(args.length==5){
            checkDBInitialized(args);
        }else if(args.length==6){
            executeCommand(args);
        }
    }

    private static void checkDBReadiness() throws Exception {
        while(true) {
            try {
                db_check_retry_count++;
                String command = getTestConnectionQuery();
                executeQuery(command);
                break;
            }catch (Exception ex){
                System.out.print(".");
                Thread.sleep(db_check_retry_sleepTime);
                if(db_check_retry_count>=db_check_retry_maxCount){
                   throw ex;
                }
            }
        }
    }

    private static void checkDBInitialized(String[] args) throws Exception {
        String command = getTestTableExistQuery(args);
        executeQuery(command);
    }

    private static void executeCommand(String[] args) throws Exception {
        String command = getExecuteCommand(args);
        executeCommand(command);
    }

    private static String getExecuteCommand(String[] args) throws Exception {
        File file = new File(args[5]);
        return FileUtils.readFileToString(file);
    }

    private static String getTestTableExistQuery(String[] args) {
        return "SELECT count(*) FROM "+args[4];
    }

    private static String getTestConnectionQuery() {
        String query = "SELECT 1 FROM DUAL ";
        if (dbUrl.contains(":jtds:sqlserver:")) {
            query = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES ";
        }
        return query;
    }

    private static void executeCommand(String command) throws Exception {
        System.out.println("############################################################");
        System.out.println("script to be executed: ");
        System.out.println(command);
        System.out.println("############################################################");
        Connection con = getConnection();
        ScriptRunner scriptExecutor = new ScriptRunner(con, true, true);
        Reader reader = new StringReader(command);
        scriptExecutor.runScript(reader);


    }

    private static void executeQuery(String query) throws Exception {
        Statement stmt = getConnectionStatement();
        ResultSet rs = stmt.executeQuery(query);
        rs.next();
        stmt.close();
    }

    private static Statement getConnectionStatement() throws ClassNotFoundException, SQLException {
        Connection conn = getConnection();
        return conn.createStatement();
    }

    private static Connection getConnection() throws ClassNotFoundException, SQLException {
        if(dbDriver==null) {
            Class.forName(System.getenv("db_driver"));
        }else{
            Class.forName(dbDriver);
        }
        return DriverManager.getConnection(dbUrl,dbUser, dbPassword);
    }
}
