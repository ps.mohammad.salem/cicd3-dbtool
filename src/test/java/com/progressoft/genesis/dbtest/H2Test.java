package com.progressoft.genesis.dbtool;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

@Ignore
public class H2Test {

    List<String> db_params = new ArrayList<String>();
    com.progressoft.genesis.dbtool.Test o=new com.progressoft.genesis.dbtool.Test();
    @Before
    public void setUp() {
        db_params.clear();
        o.dbDriver="org.h2.Driver";
        db_params.add("-u=sa");
        db_params.add("-p=");
        db_params.add("jdbc:h2:tcp://127.0.0.1/~/dbtool");
        db_params.add("-m=1");
    }

    @Test
    public void testDbConnection() throws Exception {
        o.main(db_params.toArray(new String[0]));
    }

    @Test
    public void testDbTableExist()   {

    }

    @Test
    public void executeCommandFromFile() throws Exception {
        db_params.add("-f");
        db_params.add("/home/osama/workspace/genesis/dbtool/src/test/resources/test.sql");
        o.main(db_params.toArray(new String[0]));
    }

    @Test
    public void testRun() throws Exception {
        db_params.add("-f");
        db_params.add("/home/osama/workspace/genesis/dbtool/src/test/resources/test.sql");
        o.main(db_params.toArray(new String[0]));
    }

}
